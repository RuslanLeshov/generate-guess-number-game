# generate-guess-number-game

Программа генерирует байт-код игры "Угадай число".  
За основу был взят класс GuessGame, который можно найти в той же папке, что и главный класс.  
Программа использует библиотеку OW2 ASM для генерации байт-кода.  

Для запуска необходимо иметь установленную Maven  
Сборка: `mvn clean install`  
Запуск: `java -jar target/generate-guess-number-game-1.0.jar`   
Программа создаст файл GuessGame.class в текущей директории.  
Чтобы его запустить в JVM: `java -cp . GuessGame`  
