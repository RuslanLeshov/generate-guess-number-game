package ru.nsu.ojp2020;

import java.util.Random;
import java.util.Scanner;

/*
 * Вот этот класс использовался в качестве основы для генерации класс-файла
 */
public class GuessGame {
    public static void main(String[] args) {
        Random random = new Random();
        int number = random.nextInt(100) + 1;
        boolean guessed = false;
        System.out.println("I've thought a number, try to guess!");
        Scanner scanner = new Scanner(System.in);
        while (!guessed) {
            int guess = scanner.nextInt();
            if (guess > number) {
                System.out.println("Lower");
            } else if (guess < number) {
                System.out.println("Greater");
            } else {
                guessed = true;
            }
        }

        System.out.println("Exactly! Good bye!");
    }
}
