package ru.nsu.ojp2020;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import java.io.FileOutputStream;
import java.io.IOException;

import static org.objectweb.asm.Opcodes.*;

public class Main {
    // Генерация класс-файла игры "Угадай число"
    public static void main(String[] args) throws IOException {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        String className = "GuessGame";
        // Задаем версию, модификаторы доступа, имя класса
        cw.visit(V1_8, ACC_PUBLIC, className, null, "java/lang/Object", null);

        // Создаем метод main
        MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC,
                "main", "([Ljava/lang/String;)V",
                null, null);
        mv.visitCode();

        // Создаем новый объект Random
        mv.visitTypeInsn(NEW, "java/util/Random");
        mv.visitInsn(DUP);
        mv.visitMethodInsn(INVOKESPECIAL, "java/util/Random", "<init>", "()V", false);
        mv.visitVarInsn(ASTORE, 1);

        // Вызываем метод random.nextInt(100) + 1, чтоб сгенерить число от 1 до 100
        mv.visitVarInsn(ALOAD, 1);
        mv.visitIntInsn(BIPUSH, 100);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Random", "nextInt", "(I)I", false);
        mv.visitInsn(ICONST_1);
        mv.visitInsn(IADD);
        mv.visitVarInsn(ISTORE, 2);

        // Условие остановки цикла
        mv.visitInsn(ICONST_0);
        mv.visitVarInsn(ISTORE, 3);

        // Приветственное сообщение
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitLdcInsn("I've thought a number, try to guess!");
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);

        // Создаем сканер стандартного ввода
        mv.visitTypeInsn(NEW, "java/util/Scanner");
        mv.visitInsn(DUP);
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "in", "Ljava/io/InputStream;");
        mv.visitMethodInsn(INVOKESPECIAL, "java/util/Scanner", "<init>", "(Ljava/io/InputStream;)V", false);
        mv.visitVarInsn(ASTORE, 4);

        // Начинается цикл
        Label loopStart = new Label();
        mv.visitLabel(loopStart);

        // Проверка условия выхода из цикла
        mv.visitVarInsn(ILOAD, 3);
        Label loopEnd = new Label();
        mv.visitJumpInsn(IFNE, loopEnd);

        // Читаем число от пользователя
        mv.visitVarInsn(ALOAD, 4);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Scanner", "nextInt", "()I", false);
        mv.visitVarInsn(ISTORE, 5);

        // Сравниваем числа
        mv.visitVarInsn(ILOAD, 5);
        mv.visitVarInsn(ILOAD, 2);
        Label secondCheck = new Label();
        mv.visitJumpInsn(IF_ICMPLE, secondCheck);

        // Число от пользователя больше чем загаданное, говорим что надо меньше
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitLdcInsn("Lower");
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
        // И возвращаемся к началу цикла
        mv.visitJumpInsn(GOTO, loopStart);

        // Сравниваем числа
        mv.visitLabel(secondCheck);
        mv.visitVarInsn(ILOAD, 5);
        mv.visitVarInsn(ILOAD, 2);
        Label breakLoopCondition = new Label();
        mv.visitJumpInsn(IF_ICMPGE, breakLoopCondition);

        // Число от пользователя меньше чем загаданное, говорим что надо больше
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitLdcInsn("Greater");
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
        // И возвращаемся к началу цикла
        mv.visitJumpInsn(GOTO, loopStart);

        // Устанавливаем условие выхода из цикла
        mv.visitLabel(breakLoopCondition);
        mv.visitInsn(ICONST_1);
        mv.visitVarInsn(ISTORE, 3);
        // И уходим на его проверку к началу цикла
        mv.visitJumpInsn(GOTO, loopStart);

        // Конец работы программы - сообщение об успехе
        mv.visitLabel(loopEnd);
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitLdcInsn("Exactly! Good bye!");
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);

        mv.visitInsn(RETURN);

        // Здесь переданные значения игнорируются, но метод вызвать надо
        mv.visitMaxs(3, 6);
        mv.visitEnd();

        cw.visitEnd();

        FileOutputStream fileOutputStream = new FileOutputStream("GuessGame.class");
        fileOutputStream.write(cw.toByteArray());
        fileOutputStream.close();
    }
}
